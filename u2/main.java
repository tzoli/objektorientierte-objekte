public class Main {
	
	/*
	 * Aufgabe 3
	 */
	static int f_1(int x) {
		return x;
	}
	
	static double f_2(double x) {
		return Math.pow(x, 2) + 17 * 2;
	}
	
	static double f_x(double x) {
		return (Math.pow((x - 1), 3) - 14) / 2;
	}
	
	/*
	 * Aufgabe 4
	 */
	static int fakultaet(int n) {
		int o = 1;
		for (int i = 1; i < n; i++) {
			o *= i;
			System.out.println(o);
		}
		return o;
	}

	/*
	 * Aufgabe 5
	 */
	static int f_5_i() {
		int sum = 0;
		for (int i = 0; i < 28; i++) {
			sum += Math.pow((i - 1), 2);
		}
		
		return sum;
	}
	
	static int f_5_ii() {
		int sum = 0;
		for (int i = 0; i < 100; i++) {
			sum += (i * (+ 1)) / 2;
		}
		
		return sum;
	}
	
	static int f_5_iii() {
		int sum = 0;
		for (int i = 0; i < 25; i++) {
			sum += i + 1 / i;
		}
		
		return sum;
	}
	
	/*
	 * Aufgabe 6
	 */
	static void aufgabe_6() {
		int x = 7;
		while (x < 12) {
			// Anweisung
			x++;
		}
		
		int y = 0;
		int x = 10;
		while (x > y) {
			// Anweisung
			y++;
			x--;
		}
		
		for (int a = 1024; a > 2; a = a / 2) {
			// Anweisung
		}
	}
	
	public static void main(String[] args) {
		System.out.println("f_1:");
		System.out.println(f_1(4));
		assert(f_1(4) == 5);
		System.out.println("f_2:");
		System.out.println(f_2(6));
		assert(f_2(6) == 52.0);
		System.out.println("f_x:");
		System.out.println(f_x(6));
		assert(f_x(6) == 55.5);
		
		System.out.println("Fakultät:");
		fakultaet(20);
		
		System.out.println("Aufgabe 5:");
		System.out.println(f_5_i());
		System.out.println(f_5_ii());
		System.out.println(f_5_ii());
	}
}
